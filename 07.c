#include <stdio.h>

int main(){
    
    float gradosC=0.0f;
    float gradosF=0.0f;
    
    printf("Dime una temperatura en grados centigrados\n");
    scanf("%f",&gradosC);
    getchar();
    
    gradosF = (gradosC*9/5)+32;
    
    printf("%f grados centigrados equivalen a\n%0.1f grados Farenheit",gradosC,gradosF);
    
    getchar();
    
    return 0;
    

    
}