#include "raylib.h"
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
 
    InitWindow(screenWidth, screenHeight, "raylib [textures] example - texture loading and drawing");
 
    // NOTE: Textures MUST be loaded after Window initialization (OpenGL context is required)
    Texture2D texture = LoadTexture("sprites-cat-running.png");        // Texture loading
    Vector2 pos = {0,0};
   
    Rectangle sourceRec = {0,0,512,256};
    Rectangle destRec = {screenWidth-512/2,screenHeight-256/2,512,256};
    Vector2 origin = {512,0};
    int rotation = 0;
   
    SetTargetFPS(60);
    //---------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
 
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(RAYWHITE);
 
            //DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, WHITE);
   
            //DrawTextureV(texture, pos, WHITE);
            //DrawTextureEx(texture, pos, 0.0f, 0.25f, WHITE);
           
            DrawTexturePro(texture, sourceRec, destRec, origin, (float)rotation, WHITE);
 
           
            //DrawText(FormatText("FRAME %d",frame), 360, 370, 10, GRAY);
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadTexture(texture);       // Texture unloading
 
    CloseWindow();                // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}