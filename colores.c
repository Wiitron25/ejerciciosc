#include "raylib.h"
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 1024;
    int screenHeight = 800;
 
    InitWindow(screenWidth, screenHeight, "raylib [textures] example - image loading");
 
    // NOTE: Textures MUST be loaded after Window initialization (OpenGL context is required)
 
    Image image = LoadImage("white_square.png");     // Loaded in CPU memory (RAM)
    Texture2D texture = LoadTextureFromImage(image);          // Image converted to texture, GPU memory (VRAM)
 
    UnloadImage(image);   // Once image has been converted to texture and uploaded to VRAM, it can be unloaded from RAM
    //---------------------------------------------------------------------------------------
 
    // Creamos un color de 2 formas iguales
    Color micolor = { 200, 200, 200, 255/2 };
   
    Color micolor2;
    micolor2.r = 200;
    micolor2.g = 200;
    micolor2.b = 200;
    micolor2.a = 255/2;
   
   
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(WHITE);
 
            DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, GREEN);
            DrawTexture(texture, 0, 0, RED);
            DrawTexture(texture, 0, screenHeight - texture.height, BLUE);
            DrawTexture(texture, screenWidth - texture.width, 0, YELLOW);
            DrawTexture(texture, screenWidth - texture.width, screenHeight - texture.height, LIME );
           
            DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/3, micolor);
           
           
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadTexture(texture);       // Texture unloading
 
    CloseWindow();                // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}