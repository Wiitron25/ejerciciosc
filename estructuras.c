#include "raylib.h"
 
typedef enum { LOGO = 0, TITLE, GAMEPLAY, ENDING } GameScreen;
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
   
    InitWindow(screenWidth, screenHeight, "raylib example - screens manager simple");
   
    GameScreen currentScreen = LOGO;  // 0=LOGO, 1=TITLE, 2=GAMEPLAY, 3=ENDING
   
    int framesCounter = 0;
   
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
   
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------      
        switch (currentScreen)
        {
            case LOGO:
            {
                framesCounter++;
           
                if (framesCounter > 240) currentScreen = TITLE;
               
            } break;
            case TITLE:
            {
                if (IsKeyPressed(KEY_ENTER)) currentScreen = GAMEPLAY;
               
            } break;
            case GAMEPLAY:
            {
                if (IsKeyPressed(KEY_ENTER)) currentScreen = ENDING;
               
            } break;
            case ENDING:
            {
                if (IsKeyPressed(KEY_ENTER)) currentScreen = TITLE;
           
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
       
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
       
            ClearBackground(RAYWHITE);
           
            switch (currentScreen)
            {
                case LOGO:
                {
                    DrawRectangle(0, 0, screenWidth, screenHeight, LIGHTGRAY);
                    DrawText("LOGO SCREEN", 10, 10, 30, GRAY);
                    DrawText("wait for 4 seconds...", 300, 200, 20, GRAY);
                   
                } break;
                case TITLE:
                {
                    DrawRectangle(0, 0, screenWidth, screenHeight, GREEN);
                    DrawText("TITLE SCREEN", 10, 10, 30, DARKGREEN);
                    DrawText("press ENTER for next screen", 240, 200, 20, DARKGREEN);
                   
                } break;
                case GAMEPLAY:
                {
                    DrawRectangle(0, 0, screenWidth, screenHeight, RED);
                    DrawText("GAMEPLAY SCREEN", 10, 10, 30, MAROON);
                    DrawText("press ENTER for next screen", 240, 200, 20, MAROON);
                   
                } break;
                case ENDING:
                {
                    DrawRectangle(0, 0, screenWidth, screenHeight, BLUE);
                    DrawText("ENDING SCREEN", 10, 10, 30, DARKBLUE);
                    DrawText("press ENTER for TITLE screen", 230, 200, 20, DARKBLUE);
                } break;
                default: break;
            }
       
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
   
    return 0;
}