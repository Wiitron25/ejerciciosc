#include "raylib.h"
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 500;
    int screenHeight = 500;
 
    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
    Texture2D texture = LoadTexture("Wolves-logo-heat-colors.png");
    Vector2 origin = {0,0};
   
    Rectangle rec = {0, 0, screenWidth, screenHeight};
    Color recColor = WHITE;
   
    bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
    
    
   
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        if(fadeOut)
        {
            alpha += fadeSpeed;
           
            if(alpha >= 1.0f)
            {
                alpha = 1.0f;
            }
        }
        else
        {
            alpha -= fadeSpeed;
            if(alpha <= 0.0f)
            {
                alpha = 0.0f;
            }
        }
       
        if(IsKeyPressed(KEY_SPACE))
        {
            fadeOut = !fadeOut;
        }
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(RAYWHITE);
            
            DrawTexture (texture, 0, 0, WHITE);
           
            DrawRectangleRec(rec, Fade(recColor, alpha));
           
            DrawText(FormatText("%f", alpha), 10,10, 20, WHITE);
 
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------  
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}