#include "raylib.h"
 
typedef enum { LOGO = 0, TITLE, GAMEPLAY, ENDING } GameScreen;
 
//Funciones de Update
void update_logo_screen();
void update_title_screen();
void update_gameplay_screen();
void update_ending_screen();
 
//Funciones de Draw
void draw_logo_screen();
void draw_title_screen();
void draw_gameplay_screen();
void draw_ending_screen();
 
 
//Definicion de variables globales
GameScreen currentScreen = LOGO;  // 0=LOGO, 1=TITLE, 2=GAMEPLAY, 3=ENDING
int framesCounter = 0;
int screenWidth = 800;
int screenHeight = 450;
 
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
   
    InitWindow(screenWidth, screenHeight, "raylib example - screens manager simple");
    Texture2D texture = LoadTexture("Wolves-logo-heat-colors.png");
    Vector2 origin = {0,0};
   
    Rectangle rec = {0, 0, screenWidth, screenHeight};
    Color recColor = WHITE;
   
    bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
   
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
   
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------      
        switch (currentScreen)
        {
            case LOGO:
            {
                update_logo_screen();                
            } break;
            case TITLE:
            {
                update_title_screen();
               
            } break;
            case GAMEPLAY:
            {
                update_gameplay_screen();
               
            } break;
            case ENDING:
            {
                update_ending_screen();
           
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
       
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
       
            ClearBackground(RAYWHITE);
           
            switch (currentScreen)
            {
                case LOGO:
                {
                    draw_logo_screen();
                   
                } break;
                case TITLE:
                {
                    draw_title_screen();
                } break;
                case GAMEPLAY:
                {
                    draw_gameplay_screen();
                } break;
                case ENDING:
                {
                    draw_ending_screen();
                } break;
                default: break;
            }
       
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
   
    return 0;
}
 
 
//Funciones de Update
void update_logo_screen(){
    framesCounter++;
    bool fadeOut = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;   
    if(fadeOut)
        {
            alpha += fadeSpeed;
           
            if(alpha >= 1.0f)
            {
                alpha = 1.0f;
            }
        }
        else
        {
            alpha -= fadeSpeed;
            if(alpha <= 0.0f)
            {
                alpha = 0.0f;
            }
        }
       
 
    if (framesCounter > 240) currentScreen = TITLE;
}
 
void update_title_screen(){
    if (IsKeyPressed(KEY_ENTER)) currentScreen = GAMEPLAY;
}
 
void update_gameplay_screen(){
    if (IsKeyPressed(KEY_ENTER)) currentScreen = ENDING;
}
void update_ending_screen(){
    if (IsKeyPressed(KEY_ENTER)) currentScreen = TITLE;
}
 
//Funciones de Draw
void draw_logo_screen(){
    DrawRectangle(0, 0, screenWidth, screenHeight, LIGHTGRAY);
    DrawText("LOGO SCREEN", 10, 10, 30, GRAY);
    
    DrawTexture (texture, 0, 0, WHITE);
           
    
    
    DrawText("wait for 4 seconds...", 300, 200, 20, GRAY);
}
 
void draw_title_screen(){
    DrawRectangle(0, 0, screenWidth, screenHeight, GREEN);
    DrawText("TITLE SCREEN", 10, 10, 30, DARKGREEN);
    DrawText("press ENTER for next screen", 240, 200, 20, DARKGREEN);
}
 
void draw_gameplay_screen(){
    DrawRectangle(0, 0, screenWidth, screenHeight, RED);
    DrawText("GAMEPLAY SCREEN", 10, 10, 30, MAROON);
    DrawText("press ENTER for next screen", 240, 200, 20, MAROON);
}
void draw_ending_screen(){
    DrawRectangle(0, 0, screenWidth, screenHeight, BLUE);
    DrawText("ENDING SCREEN", 10, 10, 30, DARKBLUE);
    DrawText("press ENTER for TITLE screen", 230, 200, 20, DARKBLUE);
}