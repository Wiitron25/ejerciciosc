/****************************************************************************
*
*						FINAL BATTLE (EXAM 001)
*
*					Student name: Alex Munoz
*                          Group: 1C
*
*****************************************************************************/

#include <stdio.h>		// TODO: Used for functions:
#include <stdlib.h>		// TODO: Used for functions:
#include <time.h>		// Used for function time()

void DrawTitle();
void DrawGargole();
void DrawCrazyRat();
void DrawSkeleton();
void DrawPlayerEnemyLife(int pLife, int eLife);

int main(){
	// Game variables declaration
	//-----------------------------
	int playerLife = 50;
	int playerAttack;
	int playerAction;
	int playerEscape;
	int enemyLife = 0;
	int enemyType = -1;
	int enemyAttack;
	int totalGold = 0;
	int enemyGold;
	char startKey;
    time_t;
	
	// Basic game logic

	//-------------------
	// TODO: Init random seed based on current time (1p)
    srand (time(NULL));
    
	DrawTitle(); 	// Draw main title
	
	printf("You are lost in the woods, the most horrible creatures are chasing you...\n");
	printf("Something is approaching, you can feel it, it's time to do your best!\n");
	
	printf("\nPress X to exit or any other key to START: ");
	scanf("%c", &startKey);
    getchar();
	if(startKey=='x'){
        return 0;
    }
	
	// TODO: Check if X or any other key has been pressed (1p)
	
	// NOTE: At this point starts a game LOOP
    
  do{  
  
	playerEscape = 0;			// Initialize playerEscape for the loop
	
	// Get a random enemy
    
	enemyType = rand() %3 + 0;		
    
     // Get a random integer number between 0 and 2
    
    if(enemyType==0){
         
         enemyLife = 20 + rand() %9 + 10;
         enemyAttack = rand() %9 + 2;
         
         printf("Enemy Life is %i\n",enemyLife);
         printf("Enemy Attack is %i\n",enemyAttack);
         
         DrawGargole();
          
         getchar();    
    }
    else if(enemyType==1){
         
         enemyLife = 40 + rand() %19 + 20;
         enemyAttack = rand() %13 + 4;
         
         printf("Enemy Life is %i\n",enemyLife);
         printf("Enemy Attack is %i\n",enemyAttack);
         
         
         DrawCrazyRat();
         
         getchar();
    }
    else if(enemyType==2){
         
         enemyLife = 80 + rand() %39 + 40;
         enemyAttack = rand() %17 + 6;
         
         printf("Enemy Life is %i\n",enemyLife);
         printf("Enemy Attack is %i\n",enemyAttack);

         
         DrawSkeleton();
         
         getchar();      
    }
     
	
	// TODO: Depending on enemyType, draw the right enemy, display the right message and set enemyLife (1p)
	// enemyType = 0  -->  enemyLife = 20 + rand() % 10;
	// enemyType = 1  -->  enemyLife = 40 + rand() % 20;
	// enemyType = 2  -->  enemyLife = 80 + rand() % 40;

    // EXTRA: Balance enemies life
	
	DrawPlayerEnemyLife(playerLife, enemyLife);
	
	printf("\n\nPress ENTER to START BATTLE! \n");
	getchar();
	
	// NOTE: At this point starts a battle LOOP
	 
	// TODO: Show available actions (attack, defend, escape) and read player selection (1p)
   while((playerLife>0) && (enemyLife>0) && (playerEscape == 0)){
       
    
    printf("Your life is %d\n",playerLife);
    printf("Enemy Life is %d\n",enemyLife);
    printf("What do you want to do?\n");
    printf("1. ATTACK\n");
    printf("2. DEFEND\n");
    printf("3. ESCAPE\n");
    scanf("%d", &playerAction),
    getchar();
    
	
	// EXTRA: Balance player attack power and enemies attack power
    

	playerAttack = rand() % 14 + 6;		// Player attack power is always between 6 and 20
	
	// TODO: Set enemy attack power (enemyAttack), it depends on enemyType (1p)
	// enemyType = 0  -->  value between 2 and 10
	// enemyType = 1  -->  value between 4 and 16
	// enemyType = 2  -->  value between 6 and 22
	
	// Depending on choosen action, different things happen
	//------------------------------------------------------
	if (playerAction == 1)			// Attack
	{
        playerAttack = rand() %14 + 6;
        
        printf("Your Attack Power is %i\n",playerAttack);
        
        if(enemyAttack <= playerAttack){
            
            printf("You receive %d damage\n", enemyAttack / 2);
            
            printf("And you make %d damage\n", playerAttack);
            
            getchar();
            
            playerLife = playerLife - enemyAttack / 2;
            
            enemyLife = enemyLife - playerAttack;
            
            printf("Current Player Life: %d\n\n", playerLife);
            printf("Current Enemy Life: %d\n\n", enemyLife);
            
            getchar();
      
        }
        else{
            
            printf("You receive %d damage\n", enemyAttack);
            printf("And you make %d damage\n", playerAttack / 2);
            
            getchar(); 
            
            playerLife = playerLife - enemyAttack;
            enemyLife = enemyLife - playerAttack / 2;
            
            printf("Current Player Life: %d\n\n", playerLife);
            printf("Current Enemy Life: %d\n\n", enemyLife);
            
            getchar();
        }
		// TODO: Implement attack logic (2p)
		
		// playerAttack and enemyAttack are compared, 
		// if playerAttack is greater (or equal), enemy receives de full hit (all playerAttack damage) and player only receives enemyAttack/2 damage.
		// if enemyAttack is greater, player receives de full hit and enemy only receives playerAttack/2 damage.
	}
	else if (playerAction == 2)		// Defend
	{
        
        printf("You make no damage\n");
        printf("But you receive %d damage\n", enemyAttack / 3);
        printf("You recover +6 life points\n");
        
        playerLife =  playerLife + 6;
        
        printf("Current Player Life: %d\n\n", playerLife);
		// TODO: Implement defend logic (1p)
	
		// enemy receives no damage, player receives enemyAttack/3 damage but also recovers +6 life points

	}
	else if (playerAction == 3)		// Escape
	{
		// TODO: Implement escape logic (2p)
		
		// player has a 50% probability to escape:
		playerEscape = rand() % 2;
        
        if(playerEscape == 1){
            
            printf("You didn't escape...\n");
            
            getchar();
            
            printf("You receive %d damage\n", enemyAttack);
            
            playerLife = playerLife - enemyAttack;
            
            printf("Current Player Life: %d", playerLife);
            
            getchar();
            
            
        }
        else{
            
            printf("Congratulations! You escaped!!\n");
            printf("Those monsters are too stupid\n");
            
            printf("\nPLAYER TOTAL GOLD: %i\n", totalGold);

	        printf("\nGAME OVER");
	   
           getchar();
           
           return 0;
        }
	
		// if escape, player receives no damage, if not, player receives full damage (show messages)
	}
	else
	{
        printf("Are you serious?\n");
        printf("...\n");
        printf("...\n");
        printf("...\n");
        getchar();
        system("cls");
        printf("Look!\n");
        getchar();
        printf("There's an ancient golden coin right there!\n");
        printf("We're gonna be rich!\n");
        getchar();
        system("cls");
        printf("Oops...\n");
        printf("The coin was a trap\n");
        getchar();
        printf("WATCH OUT! THE BLAD...\n");
        getchar();
        printf("oof... That hurts... A little bit...\n");
        getchar();
        printf("Is... Is this your head?\n");
        getchar();
        printf("Are you ok?... Oh... You have no head...\n");
        getchar();
        printf("...\n");
        printf("...\n");
        printf("...\n");
        getchar();
        printf("SH*T!\n\n");
       
        
        printf("GAME OVER\n");
        
        getchar();
        
         return 0;        
		// EXTRA: Any idea?	(1p)
	}
 
	// Print on screen player/enemy attacks (only if player didn't escape)
	/*if (playerEscape == 0)
	{
		// TODO: Print player/enemy attack power	(1p)
		printf("Your Attack Power: %d\n", playerAttack);
        
        printf("Enemy Attack Power: %d\n", enemyAttack);
		DrawPlayerEnemyLife(playerLife, enemyLife);
		
		printf("\n\nPress ENTER to CONTINUE! \n");
		getchar();
	}*/
   
	// Check player/enemy life to see if player died or enemy died
	if (playerLife <= 0)
	{
        system("cls");
        system("color 04");
        
		printf("\nOuch! You died... YOU DIED!!!\n");
		
		printf("\nGAME OVER...\n");
	}
	else if (enemyLife <= 0)
	{
        system("cls");
        system("color 02");
        
		printf("GREAT! You beat the monster... YOU BEAT THE MONSTER!!!\n");
		
		printf("\nCONGRATULATIONS...\n");
	
		enemyGold = 100 + enemyType * 200 + rand() % 100;
		printf("\nMonster drop a bag with money... You get +%i of Gold!\n", enemyGold);
		
		int lifePotion = rand() % 30 + 10;
		
		printf("Monster also drop a life potion... You get +%i of Life\n", lifePotion);
	
		totalGold += enemyGold;
		playerLife += lifePotion;
        
        printf("\nPress X to exit or any other key to fight another ENEMY: ");
	    scanf("%c", &startKey);
        getchar();
        
        if(startKey=='x'){
           printf("\nPLAYER TOTAL GOLD: %i\n", totalGold);
	       printf("\nGAME OVER");
	       getchar();
           return 0;      
        }
        else{
            break;
        }
	}
    
   
	
	// TODO: At this point, if player or enemy are not dead and player didn't escape, return to battle LOOP... (2p)
	
	// NOTE: At this point, if X is pressed finish, if not, get a new FIGHT!

	// EXTRA: Add some clear-screen and some colors
   }
  }while(startKey!='x');
  
	return 0;
}

void DrawTitle()
{
	printf("\n");
	printf("            787288778   42    87     87     882      20               \n");
	printf("          78887 78887 7885  2887    887   7228887  7888               \n");
	printf("           280         882   8807   887  88   788   480               \n");
	printf("           788722      482   88  97 887  88   588   784               \n");
	printf("           788  7      487   887  75887  88 77788   289               \n");
	printf("           788         082   887    887  882   88   784               \n");
	printf("           7884        8887  8887   888  881  7885  488  797          \n");
	printf("            77         77    77     77   77    77   727227            \n");
	printf("     784887      484     752820770   774078 97   8        477888 82   \n");
	printf("   7888 78887  7228887 707 28 7888 25  80 7887 488      2888  4888    \n");
	printf("    288   12  88    88     88      7  289      788       88           \n");
	printf("    788 780   88   288     88         784       88       884727       \n");
	printf("    788  4887 88777788     88         284       88       887          \n");
	printf("    788   784 885   88     88   72    284   77  88       88           \n");
	printf("   7888   45  880   888    888729     7888742   887 725  0884 7787    \n");
	printf("     77215    72    27      722         712     222557    722155      \n");
	printf("\n");		
}

void DrawGargole()
{
	printf("\n");
	printf("                 .;                    ;,.                  \n");
	printf("               ##-,.xX              =x-.=+#,                \n");
	printf("                  =#X -=           x ,XX  .                 \n");
	printf("         =-===       X- +X-;--;,+x, #       -===-           \n");
	printf("      =X-      ##    #X  .      .  .#    =#=      #X        \n");
	printf("     =  .= -+,   ,; x  +- -X; +x  +- ;= +    += .=   +      \n");
	printf("   #+ ++-=.#x .#;  -  #; * .###  * #-  ,  X+ -#=.=-X. #x    \n");
	printf("   #=X#+  -#  x-##  .xX-   +#+#    #x=  =#+=- +x   ##+x#    \n");
	printf("       #X;X  #x  Xx  .-=++x    =x+x=-   #, .#+ -x;#,        \n");
	printf("        ,#+ xx   ;   ###++=    -=+###-  -    #. ##          \n");
	printf("         Xx++X#x x#  ###+  x###   ###, -#.,X#+++X           \n");
	printf("                ###x =###.####X#--###  ###;                 \n");
	printf("                   #- .X###;+X;+###=  X=                    \n");
	printf("               x+   #+  ;###xxX##+   #.  -+                 \n");
	printf("              #  -= +,+x   -+++   -X;=- x  ##               \n");
	printf("               ##  . #  ##x    -X#+ +- . =#,                \n");
	printf("                 #x  #     #+=#=    ##  #X                  \n");
	printf("                  +##      ;#;+      +##                    \n");
	printf("                             X;-X          ++++xxxXx        \n");
	printf("                              ,# .xx    xX  ##X,    #-      \n");
	printf("                                x#+ +XX= =#+   +X   #=      \n");
	printf("                                   Xxx+xX        ###        \n");
	printf("\n");
}

void DrawCrazyRat()
{
	printf("\n");
	printf("                x+++x  ++++  +x==x.                         \n");
	printf("  =#. #X      +#  x+;+x;...+x-=X. ##      x#. ##            \n");
	printf("  X# +X #=    ##  ,#,         #-  x#     # +x x#    -;;;    \n");
	printf("  =#; .=X#=     +-=;          ,=-=,    ,X#+,.-+= =x+.;###,  \n");
	printf(" .++#=-#, ;X-.   # XX=+;  ,==+# #,  .,X=  #=-; .=  ;#x;,=   \n");
	printf("     ###x.  ;+x+## *  =+;;++  * +#+x+-   +##= =#  =#+       \n");
	printf("         #x     -#x ,=,    .=; +#=     =#,   =, =#X         \n");
	printf("          =#;=    =X+,      .+X+    =;#X   ;x   =#          \n");
	printf("           ##-     ,=##x=;x##=-     ,##    #=  +#           \n");
	printf("            #-    +=;=##,###---x.   .#    #   ##            \n");
	printf("            #=  ;X  =+=.+#-;+=  x-  -#   #,  ;#+            \n");
	printf("           #,     =X.         xx      #x,+  ,#x             \n");
	printf("           #,     -            ,     .##   ;#+              \n");
	printf("         ,#,.                          X+ ,##               \n");
	printf("         ,#  .                    .    +#x#.                \n");
	printf("         ;#+.,                    ., ;.x#-                  \n");
	printf("          =#,-;  .    ,     ,   , .=;x#=                    \n");
	printf("          =##X-=+. ;..,. , ,; . .,-X+##+                    \n");
	printf("        ## =-x##x-;x-,===,;x+.=;;+##X;= =#                  \n");
	printf("        =##  #-.##################;.#. x##                  \n");
	printf("          Xx#++x#. .=======-==-  #X++#xX.                   \n");
	printf("\n");	
}

void DrawSkeleton()
{
	printf("\n");
	printf("                      ;;,;;                       \n");
	printf("                    x+  #  +x                     \n");
	printf("                  #-    .=   -#                   \n");
	printf("                 -+      x.   +-                  \n");
	printf("                 #     ;   ,   #                  \n");
	printf("                 # .;       ,. #                  \n");
	printf("                 ## -.      - ##                  \n");
	printf("    ..           + x###   ###x +                  \n");
	printf("   #. ==        # +####   ####+ #                 \n");
	printf("   .+ . ---   -;# .x#X  -  X#x. #--               \n");
	printf("    +x,;.  --+ =#x.   .###.   .x#+ =,.            \n");
	printf("     ##,.-.  - xXXX X       X XXX+  X##XXX        \n");
	printf("    -+x#+.,-   x#x= #,# ###.# -x##  =xxX####=     \n");
	printf("  -#=..=##-.;;  ,#X =#######= X##   #++++xxX##-   \n");
	printf("  ##+-++=x#X,.-.  +# +=#-#-+ x##  +#+=++++xX###   \n");
	printf("    ##x+===X#x.,-   -       =+  ,##+==+++x##X     \n");
	printf("     .#x+=+=+##=.;-  ,#;   -+ ++#x+=++++x##.      \n");
	printf("       ##x====x#X;.-,  -X+--#xx+=+=+=x+###        \n");
	printf("      = .X#++===X#x.,-   X#x+++=+==++x##. =       \n");
	printf("    +;    ###x===+##=.,-   ##+===++x###    -==    \n");
	printf("  .++-  -,  -#X+++=x##-.-,  ;##+x+X#-  ;; .   #   \n");
	printf("  #  +  #     #X+++++##x,,-   +#XX#     #  #,.#   \n");
	printf("   +   =       #x+++++###x,,-   ##      #, X   #  \n");
	printf(" . #;,=# .     ,#xxxX#;  =#=.-,  ;       .-#   #  \n");
	printf(" ##xXX####      #==+X  -.+=+X,.-.  -       #.  #  \n");
	printf("  #+=+x##       #x-=- #x=XX x#+.,-  ;=-+-=-##x #  \n");
	printf("  #x==-+#       ##Xx#X.;.,.x####- -+++==-=,;####  \n");
	printf("  ##+X=+X##    #X====X#;,;#x-+X##X+=x##Xx##- +##  \n");
	printf("  #####xx###   #+-+XX==+++Xx+Xx=#+=#X+x===+#X X   \n");
	printf("    #######.  #XX+-+x+++++x++++XX##.X#X+x==X#;.#  \n");
	printf("      ----   ##+xXxx======+X#xX+=+## ;###=x#X-=-  \n");
	printf("              ##XxxXX#X#X#XX=++x##=    X####  +#  \n");
	printf("             ;#XXX#########X###XX#         +x##.  \n");
	printf("            .#xx+x#,        ##++=x#         --    \n");
	printf("            #=-=X#-          ##+xx##              \n");
	printf("            #-;=##            ##=--#              \n");
	printf("           ,##x+X##X#-        ##+-=#              \n");
	printf("          #;X####x;, #       ##xxx#-              \n");
	printf("          #         -X    XxX###xx#-+             \n");
	printf("          .=     +###     # .  -#+#X #            \n");
	printf("            ###XxxxX##    ;+         #            \n");
	printf("             #x=====x#X    ####X;--==             \n");
	printf("            -##Xx+==x##   ##x++xxX##              \n");
	printf("          .#==xXX+=###-  ##x=X###X##..            \n");
	printf("         ##-;--+x##xx    ##x=++==-,;x##           \n");
	printf("          #######         ###x===;,,  +#x         \n");
	printf("                           .;######xxx###         \n");
	printf("                                 ++++++           \n");
	printf("\n");	
}

// TODO: Implement function to draw player and enemy life 	(2p)
// EXTRA: Check that player/enemy life is not lower than 0	(1p)
//void DrawPlayerEnemyLife(int pLife, int eLife)

	// ...


