#include <stdio.h>

int main()
{
    int num1 = 0;
    int num2 = 0;
    
    // Leer un numero entero --------------
    printf("Introduce el primer numero: ");
    scanf("%i", &num1);
    getchar();
    system("cls");
    // ------------------------------------
    // Leer un numero entero --------------
    printf("Introduce el segundo numero: ");
    scanf("%i", &num2);
    getchar();
    // ------------------------------------
    
    if (num1 > num2)
    {
        printf("El primer numero es mayor que el segundo");
    } 
    else if (num1 < num2)
    {
        printf("El primer numero es menor que el segundo");
    }
    else
    {
        printf("Los numeros son iguales");
    }
    getchar();
    
    return 0;
}