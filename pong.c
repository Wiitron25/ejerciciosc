#include "raylib.h"
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 1024;
    int screenHeight = 768;
 
    InitWindow(screenWidth, screenHeight, "raylib [shapes] example - basic shapes drawing");
   
    SetTargetFPS(60);
    #define VELOCIDAD 5
    
    Vector2 ball = { screenWidth/2, screenHeight/2 };
    
    Vector2 ballspeed;
    
    float ballsize = 20.0f;
    
    float maxspeed = 5;
    
    Vector2 ballVelocity;
   
    ballVelocity.x = -maxVelocity;
    ballVelocity.y = -maxVelocity;

    Rectangle pala1;
    pala1.width = 20;
    pala1.height = 100;
    pala1.x = 20;
    pala1.y = screenHeight/2 - pala1.height/2;
   
    Rectangle pala2;
    pala2.width = 20;
    pala2.height = 100;
    pala2.x = screenWidth-40;
    pala2.y = screenHeight/2 - pala2.height/2;
   
    Rectangle liheSeparation;
    liheSeparation.width = 6;
    liheSeparation.height = screenHeight;
    liheSeparation.x = screenWidth/2-liheSeparation.width/2;
    liheSeparation.y = 0;
   
    //--------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------
          if (IsKeyDown(KEY_UP)) pala1.y -= VELOCIDAD;
          if (IsKeyDown(KEY_DOWN)) pala1.y += VELOCIDAD;
          
          if(pala1.y < 0){
               pala1.y =  0 ;
          }
          else if(pala1.y > screenHeight - pala1.height){
              pala1.y = screenHeight - pala1.height;
          }
          
          ball.x = ball.x + maxVelocity.x;
          ball.y = ball.y + maxVelocity.y;
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(RAYWHITE);
 
            DrawText("KKPONG", 20, 20, 20, DARKGRAY);
            
            DrawCircleV(ball, 20, RED);
 
            DrawRectangleRec(pala1,BLACK);
           
            DrawRectangleRec(pala2,BLACK);
           
            DrawRectangleRec(liheSeparation,BLACK);
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}
