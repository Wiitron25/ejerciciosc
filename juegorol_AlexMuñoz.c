#include <stdio.h>

int main(){
    
      int num = 0;
      int tiempo1 = 30;
      int tiempo2 = 0;
      int contador = 3;
      int vida = 100;
      
      
       printf("Estas dormido profundamente...\n");
       getchar();
       
       system("cls");
       printf("Cuando de pronto unas explosiones te despiertan\n");
       getchar();
      
       printf("Te das cuenta de que estan atacando vuestra base\n");
       printf("cuando ves una explosion por la ventana\n");
       getchar();
      
       printf("Que decides hacer?\n\n");
       printf("-Salir al hangar a defender la base (1)\n\n");
       printf("-Esconderte debajo de la mesa mas cercana (2)\n\n");
       printf("-Salir con las manos en alto y rendirte (3)\n\n");
       scanf("%i", &num);
       getchar();
      
       if (num == 1){
          
        system("cls");
        printf("Sales corriendo y subes al MECHA que llevas tantos años pilotando,\n");
        printf("el cual conoces como una parte de tu cuerpo\n\n");
        getchar();
        system("cls");
        system("color 02");
          
        printf("BIENVENIDO, PILOTO B0422KL\n");
        getchar();
        printf("ACTIVANDO RANGER-023A\n");
        getchar();
        printf("ENTRANDO EN MODO COMBATE\n");
        printf("BATERIA: 93/100\n");
        printf("MUNICION: 87/100\n");
        printf("DESACOPLANDO\n\n\n");
        getchar();
          
        system("cls");
        system("color 07");
        printf("El MECHA se desacopla y sales al exterior\n");
        printf("Hay unas doce aeronaves sobrevolando la base,\n");
        printf("mas varios centenares de soldados enemigos disparando contra tus aliados\n");
        printf("varios vehiculos de asalto bombardean la base por los lados\n");
        getchar();
        system("cls");
        printf("De repente llega un mensaje por el canal de emergencia\n\n");
        getchar();
          
        system("cls");
        system("color 06");
        printf("CO..IGO...OJO...(interferencias)...EFUERZOS...LLEGARAN...30 MINUTOS\n");
        getchar();
        
        system("cls");
        system("color 07");
          
        ATAQUE:
          
        printf("Vida: %d\n", vida);
          
        printf("Debes aguantar %d minutos, pero hay demasiados enemigos\n", tiempo1);
        printf("Que decides atacar primero?\n\n");
        printf("-Atacar las aeronaves (1)\n\n");
        printf("-Atacar a los pelotones (2)\n\n");
        printf("-Atacar a los vehiculos terrestres (3)\n\n");
        scanf("%i", &num);
        getchar();
    
         if (num == 1){
              
            
            while (vida > 0){
              
             system("cls");
             printf("Consigues flanquear algunas aeronaves y atacas por la retaguardia.\n");
             printf("Algunas torretas te fijan como objetivo, pero el blindaje de tu MECHA consigue resistir muchos de los impactos,\n");
             printf("aunque recibes algunos disparos, pero nada grave\n\n");
              
             tiempo2 = tiempo1 - contador;
            
             printf("Quedan %d minutos hasta los refuerzos", tiempo2);
              
             contador = 3;
              
             vida = vida - 8;
              
             tiempo1 = tiempo2;
              
             getchar();
             system("cls");
         
             }
             if (vida < 1){
         
             system("cls");
             system("color 04");
             printf("A pesar de tus esfuerzos, no consigues repeler a los enemigos.\n");
             printf("Las constantes lluvias de misiles hacen explotar tu MECHA y mueres en combate.\n");
             printf("Tanto tu como tus aliados sereis recordados con honor.\n");
             getchar();
             printf("Piensalo... Peor es nada\n");
             getchar();
             system("cls");
             break;
         
            }
            else if (tiempo1 == 0){
                  
             system("cls");
             system("color 02");
             printf("Los refuerzos llegan a tiempo, cosiguen ayudaros a despejar la zona\n");
             getchar();
             system("cls"); 
             printf("Otro mensaje llega por parte del canal de radio:\n\n");
             printf("-Enhorabuena Piloto, ha hecho un gran trabajo\n");
                  
             break;

             return 0;             
                
            }
        }   
        else if (num == 2){
              
              while (vida > 0){
              system("cls");
              printf("Los soldados no son problema para ti.\n");
              printf("Tu MECHA es bastante resistente,\n");
              printf("aunque recibes algunos disparos, pero nada grave\n\n");
              
              tiempo2 = tiempo1 - contador;
            
              printf("Quedan %d minutos hasta los refuerzos", tiempo2);
              
              contador = 3;
              
              vida = vida - 5;
              
              tiempo1 = tiempo2;
              
              getchar();
              system("cls");
            }
             else if (vida < 1){
         
                 system("cls");
                 system("color 04");
                 printf("A pesar de tus esfuerzos, no consigues repeler a los enemigos.\n");
                 printf("Las constantes lluvias de misiles hacen explotar tu MECHA y mueres en combate.\n");
                 printf("Tanto tu como tus aliados sereis recordados con honor.\n");
                 getchar();
                 printf("Piensalo... Peor es nada\n");
                 getchar();
                 system("cls");  
             
                 break;    
             
                 return 0;
         
            }  
             else if (tiempo1 == 0){
                  
                  system("cls");
                  system("color 02");
                  printf("Los refuerzos llegan a tiempo, cosiguen ayudaros a despejar la zona\n");
                  getchar();
                  system("cls");
                  printf("Otro mensaje llega por parte del canal de radio:\n\n");
                  printf("-Enhorabuena Piloto, ha hecho un gran trabajo\n");
                  
                  break;
                  
                  return 0;
                
             }  
            goto ATAQUE;
              
        } 
        else if (num == 3){
            
            
             if (vida > 0){
              system("cls");
              printf("Consigues destruir algunos tanques.\n");
              printf("Tu MECHA es fuerte, pero varios enemigos a la vez son su punto debil.\n");
              printf("Aun con exito, has recibido bastante daño, pero puedes resistir\n\n");
              
              tiempo2 = tiempo1 - contador;
            
              printf("Quedan %d minutos hasta los refuerzos", tiempo2);
              
              contador = 3;
              
              vida = vida - 20;
              
              tiempo1 = tiempo2;
              
             getchar();
              system("cls");
            }  
            else if (vida < 1){
         
                 system("cls");
                 system("color 04");
                 printf("A pesar de tus esfuerzos, no consigues repeler a los enemigos.\n");
                 printf("Las constantes lluvias de misiles hacen explotar tu MECHA y mueres en combate.\n");
                 printf("Tanto tu como tus aliados sereis recordados con honor.\n");
                 getchar();
                 printf("Piensalo... Peor es nada\n");
                 getchar();
                 system("cls");
                 
                 break;
               
                 return 0;
         
            }     
            else if (tiempo1 == 0){
                  
                  system("cls");
                  system("color 02");
                  printf("Los refuerzos llegan a tiempo, cosiguen ayudaros a despejar la zona\n");
                  getchar();
                  system("cls");
                  printf("Otro mensaje llega por parte del canal de radio:\n\n");
                  printf("-Enhorabuena Piloto, ha hecho un gran trabajo\n"); 
                  break;

            }                
            goto ATAQUE;           
            }
         
             
    }        
    else if (num == 2){
           
           system("cls");
          system("color 04");
          printf("Consigues ponerte a salvo de los ataques,\n");
          printf("pero sin tu ayuda los enemigos entran en la base.\n");
          printf("os atrapan a ti y al resto de supervivientes\n");
          printf("Tu destino se decide con un 'piedra, papel, tijera'\n\n");
          printf("-Piedra (1)\n");
          printf("-Papel (2)\n");
          printf("-Tijera (3)\n");
          scanf("%i", &num);
          getchar();
           
          if(num > 0){
               
               system("cls");
               
               printf("Da igual lo que decidas, ellos tienen la pistola\n\n");
               printf("********GAME OVER********");
               getchar();
               system("cls");
               
            
        
            return 0;   
            } 
           
    }         
    else if (num == 3){
            
         system("cls");
         system("color 04");
         printf("Admites la derrota y sales con las manos en alto...\n\n");
         getchar();
         printf("Pero tus atacantes no parecen querer prisioneros\n\n");
         printf("Dos disparos en la cabeza terminan contigo\n");
         printf("********GAME OVER********");
         getchar();
         system("cls");
            
        return 0;
    } 
    
   }