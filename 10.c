#include <stdio.h>

int main(){
    
    float angulo1=0;
    float angulo2=0;
    float angulo3=0;
    
    printf("Dame un angulo\n");
    scanf("%f",&angulo1);
    getchar();
    
    printf("Dame otro angulo\n");
    scanf("%f",&angulo2);
    getchar();
    
    angulo3 = 180 - (angulo1 + angulo2);
    
    printf("El tercer angulo es de %0.1f grados",angulo3);
    
    getchar();
    
    return 0;
    
    
}