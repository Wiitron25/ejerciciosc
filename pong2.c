#include "raylib.h"
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 1024;
    int screenHeight = 768;
 
    InitWindow(screenWidth, screenHeight, "raylib [shapes] example - basic shapes drawing");
   
    SetTargetFPS(60);
   
    // Variable que gestiona los datos de la pala1
    Rectangle pala1;
    pala1.width = 20;
    pala1.height = 100;
    pala1.x = 20;
    pala1.y = screenHeight/2 - pala1.height/2;
   
    // Variable que gestiona los datos de la pala2
    Rectangle pala2;
    pala2.width = 20;
    pala2.height = 100;
    pala2.x = screenWidth-40;
    pala2.y = screenHeight/2 - pala2.height/2;
   
    // Variable que gestiona la linea de separación
    Rectangle liheSeparation;
    liheSeparation.width = 6;
    liheSeparation.height = screenHeight;
    liheSeparation.x = screenWidth/2-liheSeparation.width/2;
    liheSeparation.y = 0;
   
    // Variable que gestiona la velocidad de las palas
    float velocidad = 5.0f;
   
    // Variable que gestiona la bola
    Vector2 ball = {screenWidth/2,screenHeight/2};
    float ballSize = 30.0f;
   
    float maxVelocity = 5;
   
    Vector2 ballVelocity;
   
    ballVelocity.x = -maxVelocity;
    ballVelocity.y = -maxVelocity;
    
    int punt1 = 0;
    
    int punt2 = 0;
    
    
   
    //--------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
       
        // Mirar si ha pulsado Arriba o abajo
        if (IsKeyDown(KEY_S)) pala1.y += velocidad;
        if (IsKeyDown(KEY_W)) pala1.y -= velocidad;    
       
        // Mirar los limites de la pala 1
        if(pala1.y<0){
            pala1.y = 0;
        }else if(pala1.y>(screenHeight-pala1.height)){
            pala1.y = screenHeight-pala1.height;
        }
        
        if (IsKeyDown(KEY_DOWN)) pala2.y += velocidad;
        if (IsKeyDown(KEY_UP)) pala2.y -= velocidad; 
        
        if(pala2.y<0){
            pala2.y = 0;
        }else if(pala2.y>(screenHeight-pala2.height)){
            pala2.y = screenHeight-pala2.height;
        }
       
        // Calcular la nueva posición de la pelota
        ball.x = ball.x + ballVelocity.x;
        ball.y = ball.y + ballVelocity.y;
        
         if((ball.x - ballSize) < 0){
            ball.x = ballSize;
            ballVelocity.x *= -1;
            punt2++;
            
        }
       
        //Miro que no se pase en y (negativo)
        if((ball.y - ballSize) < 0){
            ball.y = ballSize;
            ballVelocity.y *= -1;
        }
       
        //Miro que no se pase en x (screenWidth)
        if((ball.x + ballSize) > screenWidth){
            ball.x = screenWidth- ballSize;
            ballVelocity.x *= -1;
            punt1++;
            
        }
       
        //Miro que no se pase en y (screenHeight)
        if((ball.y + ballSize) > screenHeight){
            ball.y = screenHeight- ballSize;
            ballVelocity.y *= -1;
        }
        
        if(CheckCollisionCircleRec(ball, ballSize, pala1)){
            ball.x =  pala1.x + ballSize + pala1.width;
            ballVelocity.x *= -1;
        }  
        if(CheckCollisionCircleRec(ball, ballSize, pala2)){
            ball.x = pala2.x - ballSize - pala2.width;
            ballVelocity.x *= -1;
        }
        
        /*if(ball.x = 0){
         punt1 = punt1 += 1;
        }*/
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            // Limpio pantalla
            ClearBackground(RAYWHITE);
 
            // Pinto Texto
            DrawText("KKPONG", 20, 20, 20, DARKGRAY);
            
            DrawText(FormatText("%i", punt1), 100, 40, 40, LIGHTGRAY);
            DrawText(FormatText("%i", punt2), (screenWidth - 100), 40, 40, LIGHTGRAY); 

 
            // Pinto Pala 1
            DrawRectangleRec(pala1,BLACK);
           
            // Pinto Pala 2
            DrawRectangleRec(pala2,BLACK);
           
            // Pinto Tablero
            DrawRectangleRec(liheSeparation,BLACK);
           
            // Pinto Pelota
            DrawCircleV(ball,ballSize,RED);
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}