#include <stdio.h>

int main(){
    
    float sec=0;
    float min=0;
    float hor=0;
    
    printf("Dame una cantidad de segundos\n");
    scanf("%f",&sec);
    getchar();
    
    min = sec / 60;
    
    hor = sec / 3600;
    
    printf("%0.1f segundos son %0.2f minutos y %0.4f horas",sec,min,hor);
    
    getchar();
    
    return 0;
    
    
}

