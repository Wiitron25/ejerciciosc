#include "raylib.h"

typedef enum direccion_t{
    DERECHA,
    IZQUIERDA,
    ABAJO,
    ARRIBA,
}direccion;
    

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 900;
    int screenHeight = 600;
    int x = 0;
    int y = 0;
    InitWindow(screenWidth, screenHeight, "ejemplo raylib");
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        // TODO: Update your variables here
        //----------------------------------------------------------------------------------

        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

            ClearBackground(RED);

            DrawText("...", x, y, 40, YELLOW);

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------   
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}