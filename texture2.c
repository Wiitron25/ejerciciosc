#include "raylib.h"
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
 
    InitWindow(screenWidth, screenHeight, "raylib [textures] example - texture loading and drawing");
 
    // NOTE: Textures MUST be loaded after Window initialization (OpenGL context is required)
    Texture2D texture = LoadTexture("resources/sprites-cat-running.png");        // Texture loading
    Vector2 pos = {0,0};
   
    Rectangle frameRec = {0,0,512,256};
    int frame=0;
    int fps=0;
    //---------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        fps++;
        if(fps>30){
            fps=0;
            frame++;
            frame = frame%4;
        }
       
        frameRec.x = frame*512;
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(RAYWHITE);
 
            //DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, WHITE);
   
            //DrawTextureV(texture, pos, WHITE);
            //DrawTextureEx(texture, pos, 0.0f, 0.25f, WHITE);
           
            DrawTextureRec(texture, frameRec, pos, WHITE);
 
           
            DrawText("this IS a texture!", 360, 370, 10, GRAY);
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadTexture(texture);       // Texture unloading
 
    CloseWindow();                // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}