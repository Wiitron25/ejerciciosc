#include <stdio.h>

int main (){
    
     int num;
    
    
    
    INICIO:
    
     printf("/*****************************************/\n");
     printf("/**EL DESPERTAR DE LA PRINCESA DE LLAMAS**/\n");
     printf("/*****************************************/\n");
     getchar();
    
     printf("   En esta historia te pones en la piel de Kyle Ember\n"); 
     printf("   un joven que clama venganza por la muerte de sus padres\n");
     printf("   por parte del General Supremo del Imperio Negro, Nigai.\n\n");
     printf("   Despues de que el Imperio descubriese el gran poder que ocultaba tu hermana Kyra,\n");
     printf("   tu y tus amigos sufristeis un ataque por parte de uno de los 3 Dioses que sirven al imperio,\n");
     printf("   quien se llevo a tu hermana al Cuartel General del Imperio, el conocido como Centro del Universo...\n");
     printf("   Y bien...Donde esta ese sitio?\n\n");
     printf("   Pues bien, la base esta literalmente casi en el centro de la galaxia.\n\n");
     printf("   Bueno, la cosa esta en que tras largos viajes, duras peleas y blablabla...\n");
     printf("   consigues llegar al Centro del Universo, decides a rescatar a Kyra y llevarla a casa...\n\n");
     getchar();
    
     printf("..............antes de que pierda el control..............\n\n");
     printf("................Pulsa ENTER para comenzar.................");
     getchar();
     system("cls");
     
     printf("Vuestra nave ha conseguido infiltrarse en uno de los hangares\n");
     printf("Pero no teneis un plan concreto y unos soldados se acercan a la nave\n");
     printf("Que decides hacer?\n\n");
     getchar();
    
     printf("-Esperar a que se acerquen lo suficiente y tenderles una emboscada (1)\n\n");
     printf("-Rezar para que pasen de largo, aunque la nave resulte muy sospechosa (2)\n\n");
     printf("-Salir de la nave y atacarlos por sorpresa (3)\n\n");
     scanf("%i", &num);
    
     getchar();
     system("cls");
    
     if(num == 1){
        
        printf("Los soldados comienzan a rodear la nave, uno de ellos fuerza el codigo de la puerta y consigue abrirla\n");
        printf("Consigues pillarle por sorpresa y acabas rapido con el, tus companeros hacen lo mismo con el resto...\n");
        printf("pero tu recibes algun golpe, aunque no demasiado fuerte.\n");  
        getchar();
        
        system("cls");
        printf("Tras despejar el camino, os meteis por la entrada mas cercana.\n");
        printf("Llegais a una bifurcacion, pero no conoceis el lugar\n");
        printf("-Por donde vamos?\n\n");
        getchar();
        
        printf("-Izquierda (1)\n\n");
        printf("-Derecha (2)\n\n");
        scanf("%i", &num);
        getchar();
        
        if(num == 1)
        {
            printf("No encontrais ningun peligro, pero llegais a una puerta...\n");
            printf("Decides abrirla?\n\n");
            printf("-Si (1)\n\n");
            printf("-No, volvemos atras (2)\n");
            scanf("%i", &num);
            
            if(num == 1){
                 
                system("color 04");
         
                printf("La puerta se abre sola\n");
                printf("entras por ella pero te llevas una sorpresa...\n");
                printf("Te encuentras con un peloton del Escuadron Elemental mas otro numeroso grupo de soldados\n");
                printf("dispuesto a exterminaros\n");
                printf("...\n");
                printf("Ninguno de vosotros seria capaz de soportar el ataque de todos los enemigos\n");
                getchar();
                
                printf("-Lo siento... Kyra...");
                getchar();
                
                system("cls");
                printf("Al sentir tu muerte, Kyra pierde el control...\n");
                printf("lo que procova la destruccion de toda la base\n");
                getchar();
                
                system("cls");
                printf("***GAME OVER***\n");
                
                goto INICIO;
                system("cls");
                
            }
            else if(num == 2)
            {
             printf("Volveis a la bifurcacion\n");
             printf("elegis el camino de la izquierda");
             printf("...");
             printf("...");
             printf("...");
             printf("...");
             printf("...");
             printf("...");
             printf("...");
             printf("...");
             printf("...");
             printf("El pasillo parece no acabarse nunca, pero al final llegais a una gran sala al final de unas escaleras\n");
             getchar();
             system("cls");
             printf("Parece como si la falta de soldados fuese a propósito...\n");
             printf("Al final de la sala, el General Supremo os espera tranquilamente,\n");
             printf("sentado al pie de un pedestal en el que Kyra se encuentra encadenada\n");
             system("cls");
             
             printf("tras ella, un gran ventanal por el cual pasa la luz de las miles de estrellas de la galaxia que rodean\n");
             printf("la base del imperio mas poderoso conocido hasta ahora\n");
             printf("y ahora...\n");
             getchar();
             
             printf("te encuentras ante su lider\n");
             printf("dispuesto a recuperar a tu hermana\n");
             getchar();
             
             system("cls");
             printf("El General se pone en pie y te mata");
             printf("game over");
             
             
            
            
            }
            
         }   
     }
     else if(num == 2){
         
        system("color 04");
        
        printf("Los soldados sospechan aun mas de la nave y deciden pedir refuerzos.\n");
        printf("En pocos segundos os encontrais rodeados\n");
        printf("Decides rendirte?\n\n");
        getchar();
        
        printf("-Si (1)\n\n");
        printf("-No, pelearemos (2)\n\n");
        scanf("%i", &num);
        getchar();
        
        if(num == 1){
            system("cls");
            system("color 04");
            printf("Decidis rendiros, los soldados estan armados con material que es capaz de acabar con vosotros\n");
            printf("aunque hagais uso de vuestros poderes\n");
            printf("-Lo siento... Kyra.\n\n");
            getchar();
            printf("***GAME OVER***\n");
            getchar();
            system("cls");
            goto INICIO;
      
        }
        
        else if(num == 2){
            
           system("cls");
           system("color 04");
           
           
            printf("-LARGO DE MI CAMINO!!!\n");
            printf("No vas a dejar que unos soldados se entrometan entre tú y tu hermana\n");
            printf("asi que entre todos conseguis abriros paso\n");
            printf("-No sois rivales para mi.");
            getchar();
            system("cls");
            printf("Tras despejar el camino, os meteis por la entrada mas cercana.\n");
            printf("Llegais a una bifurcacion, pero no conoceis el lugar\n");
            getchar();
            bifurcacion:
            
            printf("-Por donde vamos?\n\n");
            printf("-Izquierda (1)\n\n");
            printf("-Derecha (2)\n\n");
            scanf("%i", &num);
            getchar();
        
         if(num == 1){
             
            
            system("color 07");
            printf("No encontrais ningun peligro, pero llegais a una puerta...\n");
            printf("Decides abrirla?\n\n");
            printf("-Si (1)\n\n");
            printf("-No, volvemos atras (2)\n");
            scanf("%i", &num);
            getchar();
            
            if (num == 1){
                
                 system("color 04");
         
                printf("La puerta se abre sola\n");
                printf("entras por ella pero te llevas una sorpresa...\n");
                printf("Te encuentras con un peloton del Escuadron Elemental mas otro numeroso grupo de soldados\n");
                printf("dispuesto a exterminaros\n");
                printf("...\n");
                printf("Ninguno de vosotros seria capaz de soportar el ataque de todos los enemigos\n");
                getchar();
                
                printf("-Lo siento... Kyra...");
                getchar();
                
                system("cls");
                printf("Al sentir tu muerte, Kyra pierde el control...\n");
                printf("lo que procova la destruccion de toda la base\n");
                getchar();
                
                system("cls");
                printf("***GAME OVER***\n");
            }
            else if (num == 2){
                
                goto bifurcacion;
            }            
         }    
         else if(num == 2)
         {
             
            system("cls");
            printf("Volveis a la bifurcacion\n");
            printf("elegis el camino de la izquierda");
            printf("...");
            printf("...");
            printf("...");
            printf("...");
            printf("...");
            printf("...");
            printf("...");
            printf("...");
            printf("...");
            printf("El pasillo parece no acabarse nunca, pero al final llegais a una gran sala al final de unas escaleras\n");
            
          
            
         }
            
     }
     else if(num == 3){
        system("cls");
        system("color 02");
           
        printf("Los barreis rapido y conseguis avanzar\n");
        printf("Parece que nadie se ha dado cuenta\n");
        printf("-Hacia donde vamos?\n\n");
        printf("-Directamente hacia Kyra, en la zona central de la base (1)\n\n");
        printf("-Salir por la parte trasera del hangar, evitando los peligros(2)\n\n");
        scanf("%d", &num);
        getchar();
        
        if (num == 1){
            
            printf("Conseguis llegar a la zona central de la base, reteniendo a los soldados imperiales\n");
            printf("Tus compañeros te consiguen tiempo peleando con los cientos de soldados mientras tu vas a rescatar a tu hermana\n");
            printf("pero todavia te queda un desafio antes de rescatar a Kyra...\n");
            getchar();
            printf("y pronto te encuentras con ese desafio");
            getchar();
            printf("sentado al pie de un pedestal en el que Kyra se encuentra encadenada, te espera el General Supremo, Nigai\n");
            system("cls");
             
            printf("tras ella, un gran ventanal por el cual pasa la luz de las miles de estrellas de la galaxia que rodean\n");
            printf("la base del imperio mas poderoso conocido hasta ahora\n");
            printf("y ahora...\n");
            getchar();
             
            printf("te encuentras ante su lider\n");
            printf("dispuesto a recuperar a tu hermana\n");
            getchar();
             
            system("cls");
            printf("El General se pone en pie y te mata");
            printf("game over");
             
        }
        
        
        
       
     }
      
      getchar();
      
      
      return 0;
      
      
      
      
      
    
}