#include <stdio.h>

int main(){
    
    int numfibonacci=0;
    int contador;
    int antepenultimo, penultimo, actual;
    
   
    printf("Cuantos numeros de fibonacci quieres?\n");
    scanf("%d", &numfibonacci);
    getchar();
    
    antepenultimo = 0;
    penultimo = 1;
    
    printf("%d %d",antepenultimo,penultimo);
    
    contador = 0;
     while(contador<numfibonacci){
        actual = antepenultimo + penultimo;
        printf("%d ",actual);
        antepenultimo = penultimo;
        penultimo = actual;
        contador++;       
    }
    
    getchar();
    
    return 0;
    
     
}